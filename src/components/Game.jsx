import React, { useEffect, useState } from "react";
import { calculateWinner } from "../utils/functions";
import Board from "./Board";

const Game = () => {
    const [history, setHistory] = useState([
        {
          squares: Array(9).fill(null),
        },
      ]);
      const [stepNumber, setStepNumber] = useState(0);
      const [xIsNext, setXIsNext] = useState(true);
      const [winnerPath, setWinnerPath] = useState([]);
        
      useEffect(() => {
        const current = history[stepNumber];
        const winner = calculateWinner(current.squares);
        if (winner) {
          setWinnerPath(winner.winningPath);
        } else {
          setWinnerPath([]);
        }
      }, [history, stepNumber]);

      function handleClick(i) {
        const newHistory = history.slice(0, stepNumber + 1);
        const current = newHistory[newHistory.length - 1];
        const squares = current.squares.slice();
        if (calculateWinner(squares)?.winner || squares[i]) {
          return;
        }
        squares[i] = xIsNext ? 'X' : 'O';
        setHistory(
          newHistory.concat([
            {
              squares: squares,
            },
          ])
        );
        setStepNumber(newHistory.length);
        setXIsNext(!xIsNext);
      }
    
      const jumpTo = (step) => {
        setStepNumber(step);
        setXIsNext(step % 2 === 0);
      }
    
      const current = history[stepNumber];
      const winner = calculateWinner(current.squares);
      const isTie = !winner && current.squares.every(square => square !== null);

      const moves = history.map((_, move) => {
        const desc = move ? `Go to move #${move}` : 'Go to game start';
        return (
          <li key={move}>
            <button className="btn" onClick={() => jumpTo(move)}>{desc}</button>
          </li>
        );
        
      });
    
      let status;
      if (winner) {
        status = `Winner: ${winner.winner}`;
      } else if(isTie) {
        status = "Nobody wins. It's a tie!"
      } else{
        status = `Next player: ${xIsNext ? 'X' : 'O'}`;
      }
    
      return (
        <div className="game">
            <div className="game-info">
              <div className={isTie ? "tie-text" : winner && "winner-text"}>{status}</div>
            </div>
            <div className="game-board">
            <Board squares={current.squares} onClick={handleClick} winningSquares={winnerPath}/>
            </div>
            <ol>{moves}</ol>
        </div>
      );
    }
export default Game;