import React from "react";

const Square = ({ value, onClick, isWinningSquare }) => {
  console.log(isWinningSquare);
  return (
    <button className={"square " + (isWinningSquare ? "winner-square" : "")} onClick={onClick}>
      {value}
    </button>
  );
};

export default Square;
