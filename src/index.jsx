import './index.css';
import Game from './components/Game';
import React from 'react';
import ReactDOM from 'react-dom'

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <Game />
);



